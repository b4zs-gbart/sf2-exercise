<?php

namespace AppBundle\Service;


use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\KernelEvent;

class ViewCounter
{
    use ContainerAwareTrait;

    public function onKernelController(FilterControllerEvent $event)
    {
        $slug = $event->getRequest()->attributes->get('slug', false);
        if (!$slug) {
            return;
        }
        $this->storeView($slug);
    }

    private function storeView($url)
    {
        $clientId = $this->getClientId();

        if (!$clientId) {
            return;
        }

        $this->getConnection()->executeQuery(
            'INSERT INTO `view_count` SET '
            .'`url` = :url, `start` = CONCAT(SUBSTR(NOW(), 1, 13), "-00-00"), timespan = 3600, `client` = :client, `count` = 1 '
            .'ON DUPLICATE KEY UPDATE count = count+1;',
            array(
                'url'       => $url,
                'client'    => $clientId,
            )
        );
    }

    public function fetchViewCountForUrl($url)
    {
        $slug = ltrim($url, '/');

        $stmt = $this->getConnection()->executeQuery(
            'SELECT SUM(count) as cnt FROM view_count WHERE url = :url',
            array(
                'url' => $slug,
            )
        );
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $row['cnt'];
    }

    private function getConnection()
    {
        return $this
            ->container
            ->get('doctrine.orm.default_entity_manager')
            ->getConnection();
    }

    private function getClientId()
    {
        $request = $this->container->get('request_stack')->getMasterRequest();
        if ($request->getSession() && $request->getSession()->getId()) {
            return $request->getSession()->getId();
        } else {
            $ip = explode('.', $request->getClientIp());
            if (4 === count($ip)) { // only if ipv4
                $ip[1] = 'xxx'; //avoid storing complete IP addresses in database
            }
            return implode('.', $ip);
        }
        
        return null;
    }


}