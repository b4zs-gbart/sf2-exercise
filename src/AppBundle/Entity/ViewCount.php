<?php

namespace AppBundle\Entity;

/**
 * ViewCount
 */
class ViewCount
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $count;

    /**
     * @var \DateTime
     */
    private $start;

    /**
     * @var integer
     */
    private $timespan;

    /**
     * @var string
     */
    private $client;

    /**
     * @var string
     */
    private $url;

    public function getId()
    {
        return $this->id;
    }

    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function setTimespan($timespan)
    {
        $this->timespan = $timespan;

        return $this;
    }

    public function getTimespan()
    {
        return $this->timespan;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setClient($client)
    {
        $this->client = $client;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }
}

