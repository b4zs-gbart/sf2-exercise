<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/{slug}", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $viewCount = $this->container->get('app.view_counter')->fetchViewCountForUrl($request->get('slug'));
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir'      => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'view_count'    => $viewCount,
        ]);
    }
}
